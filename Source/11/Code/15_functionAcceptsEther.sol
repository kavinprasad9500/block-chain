//Buy Token & Sending Ether to Wallet Notes: Tx - deployadd(Rx-2)
pragma solidity ^0.5.1;

contract myContract
{
    mapping(address => uint) public balances;
    address payable wallet;

    constructor(address payable Wallet) public
    {
        wallet = Wallet;
    }
    function buyToken() public payable
    {
        balances[msg.sender]+=1;
        wallet.transfer(msg.value);
    }
}