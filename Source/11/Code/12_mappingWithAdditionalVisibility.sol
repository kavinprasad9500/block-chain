pragma solidity ^0.5.1;

contract myContract
{
    uint public peopleCount;

    mapping(uint => Person) public people;

    struct Person
    {
        uint id;
        string name;
        string designation;
    }

    function addPerson(string memory name, string memory designation) public
    {
        incrementCount();
        people[peopleCount] = Person(peopleCount, name, designation);
    }

    function incrementCount() internal
    {
        peopleCount+=1;
    }
}