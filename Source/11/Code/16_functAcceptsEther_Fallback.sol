//Public can be called from inside/outside the contract, external can be called only from external
pragma solidity 0.5.1;

contract myContract
{
    mapping(address => uint256) public balances;
    address payable wallet;

    constructor(address payable Wallet) public
    {
        wallet = Wallet;
    }

    function() external payable
    {
        buyToken();
    }

    function buyToken() public payable
    {
        balances[msg.sender]+=1;
        wallet.transfer(msg.value);
    }
}   