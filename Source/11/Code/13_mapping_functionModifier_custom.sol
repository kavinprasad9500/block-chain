//Only Admin can add person
pragma solidity ^0.5.1;

contract myContract
{
    uint public peopleCount;

    mapping(uint => Person) public people;

    address admin;

    modifier onlyAdmin()
    {
        require(msg.sender == admin);
        _;
    }

    struct Person
    {
        uint id;
        string name;
        string designation;
    }

    constructor() public
    {
        admin = msg.sender;
    }

    function addPerson(string memory name, string memory designation) public onlyAdmin
    {
        incrementCount();
        people[peopleCount] = Person(peopleCount, name, designation);
    }

    function incrementCount() internal
    {
        peopleCount+=1;
    }
}