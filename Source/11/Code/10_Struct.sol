pragma solidity ^0.5.1;

contract myContract
{
    Person[] public people;

    uint public peopleCount;

    struct Person
    {
        string name;
        string designation;
    }

    function addPerson(string memory name, string memory designation) public
    {
        people.push(Person(name,designation));
        peopleCount +=1;
    }
}