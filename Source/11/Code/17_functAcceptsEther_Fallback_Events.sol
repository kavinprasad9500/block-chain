//Events are way for external consumers to listen for things happen on smart contract
//consumers can subscribe to event to wait for some event
//anyone connected to blockchain will know is token is been purchased
pragma solidity 0.5.1;

contract myContract
{
    mapping(address => uint256) public balances;
    address payable wallet;

    event Purchase(address indexed buyer, uint amount);

    constructor(address payable Wallet) public
    {
        wallet = Wallet;
    }

    function() external payable
    {
        buyToken();
    }

    function buyToken() public payable
    {
        balances[msg.sender]+=1;
        wallet.transfer(msg.value);
        emit Purchase(msg.sender,1);
    }
}   