//Only after the time, contract will execute
//www.epochconverter.com
pragma solidity ^0.5.1;

contract myContract
{
    uint public peopleCount;

    mapping(uint => Person) public people;

    uint openingTime = 1647263992;

    modifier onlyWhileOpen()
    {
        require(block.timestamp >=  openingTime);
        _;
    }

    struct Person
    {
        uint id;
        string name;
        string designation;
    }


    function addPerson(string memory name, string memory designation) public onlyWhileOpen
    {
        incrementCount();
        people[peopleCount] = Person(peopleCount, name, designation);
    }

    function incrementCount() internal
    {
        peopleCount+=1;
    }
}