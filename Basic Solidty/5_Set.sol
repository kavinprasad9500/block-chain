pragma solidity ^0.4.24;

contract myContract
{
    string value="Welcome Miners !";
    
    function get() public view returns(string)
    {
        return value;
    }

    function set(string value2) public
    {
        value = value2;
    }
}