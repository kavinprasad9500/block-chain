pragma solidity 0.5.1;

import "./Math.sol";

contract myContract
{
    uint public value;

    function calculate(uint value1, uint value2) public
    {
        value = Math.divide(value1,value2);
    }
}