pragma solidity ^0.5.1;

contract myContract
{
    string value;

    constructor() public
    {
        value = "Welcome Miners !";
    }
    
    function get() public view returns(string memory)
    {
        return value;
    }

    function set(string memory value2) public
    {
        value = value2;
    }
}