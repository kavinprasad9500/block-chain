pragma solidity 0.5.1;

library Math
{
    function divide(uint a, uint b) internal pure returns (uint)
    {
        require(b>0);
        uint c = a/b;
        return c;
    }
}
contract myContract
{
    uint public value;

    function calculate(uint value1, uint value2) public
    {
        value = Math.divide(value1,value2);
    }
}