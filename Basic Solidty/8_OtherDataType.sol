pragma solidity ^0.5.1;

contract myContract
{
    string public constant stringValue = "Welcome to Miners !";
    bool public boolValue = true;
    int public integerValue = -100;
    uint public UnSignedIntValue = 200;
    uint8  public unSignedInt8Value = 8;
}